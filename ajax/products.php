<?php
require('../bd/config.php');

$sql = 'select * from products';

$result = mysqli_query($db, $sql);

$response = array();
$response['status'] = 'error';
$response['message'] = '';
$response['data'] = '';

$data =
    '<table>
     <tbody>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Accion</th>
        </tr>';

if (!$result) {
    $response['message'] = 'error al cargar los productos';
    die('error al cargar los productos');
}

if (mysqli_num_rows($result) < 0) $response['message'] = 'no hay datos';
else while ($row = mysqli_fetch_array($result)) {
    $data  .= '<tr>
    <td>' . $row['id'] . '</td>
    <td>' . $row['name'] . '</td>
    <td>
        <a href="#">Eliminar</a>
        <a href="#">Editar</a>
    </td>
    </tr>';
}

$data .= '</tbody></table>';

$response['data'] = $data;
$response['status'] = 'success';

echo json_encode($response);
