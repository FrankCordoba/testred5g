<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Productos</title>
</head>

<body>
    <div>
        <h1 style="text-align: center;">
            <a style="text-decoration: none; font-size: 15px; position: relative; bottom: 5px; right: 10px;" href="../index.php">
                Regresar
            </a>
            Productos Inicio
        </h1>
    </div>

    <a style="text-decoration: none; font-size: 15px; position: relative; bottom: 5px; right: 0px;" href="./addProducto.php">
        Add
    </a>
    <div id="table">

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            loadProducts();
        });

        function loadProducts() {
            $.get('../ajax/products.php', {}, function(data, error) {
                var products = JSON.parse(data);

                if (products.status === 'success') {
                    $('#table').html(products.data)
                } else {
                    alert(products.message)
                }
            });
        }
    </script>

</body>

</html>